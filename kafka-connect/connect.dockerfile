FROM confluentinc/cp-kafka-connect-base:7.3.1


RUN confluent-hub install --no-prompt debezium/debezium-connector-mysql:latest

COPY ./entrypoint.sh /home/appuser

COPY anywhere-avro-converter-1.0.0.jar /usr/share/confluent-hub-components//confluentinc-kafka-connect-s3/lib/
COPY anywhere-schema-registry-client-1.0.0.jar /usr/share/confluent-hub-components//confluentinc-kafka-connect-s3/lib/

CMD ["bash", "entrypoint.sh"]