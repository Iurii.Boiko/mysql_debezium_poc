#!/bin/bash

docker build -t mysql-debezium-kafka-connect -f ./kafka-connect/connect.dockerfile ./kafka-connect

docker-compose -f kafka-connect-mysql.yml up
