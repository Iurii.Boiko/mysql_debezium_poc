#!/bin/bash

CONNECT_URL=http://localhost:8083
KSQLDB_URL=http://ksqldb-server:8088

echo "Creating MySql Debezium connector"
curl -X POST -H 'Content-Type:application/json' --data @"./mysql-connector.json" http://localhost:8083/connectors/