# POC for testing MySQL Debezium connector


To run this POC, you need to have Docker and Docker Compose installed.

Steps to run:
1. Start Kafka, Kafka Connect, Kafka Connect UI and MySQL by running:
    ```bash
    $ ./start.sh
    ```
2. Log into MySQL and create a database and a table:
    ```bash
    docker exec -it mysql_debezium_poc-mysql-1 bash
    bash-4.4# mysql -uroot -proot
    mysql> CREATE DATABASE IF NOT EXISTS connect_test;
    mysql> USE connect_test;
     
    mysql> CREATE TABLE IF NOT EXISTS test (
     ->  id serial NOT NULL PRIMARY KEY,
     ->  name varchar(100),
     ->  email varchar(200),
     ->  department varchar(200),
     ->  modified timestamp default CURRENT_TIMESTAMP NOT NULL,
     ->  INDEX `modified_index` (`modified`)
     ->  );

    mysql> INSERT INTO test (name, email, department) VALUES ('alice', 'alice@abc.com', 'engineering');
    mysql> INSERT INTO test (name, email, department) VALUES ('bob', 'bob@abc.com', 'sales');
    mysql> INSERT INTO test (name, email, department) VALUES ('bob', 'bob@abc.com', 'sales');
    mysql> INSERT INTO test (name, email, department) VALUES ('bob', 'bob@abc.com', 'sales');
    mysql> INSERT INTO test (name, email, department) VALUES ('bob', 'bob@abc.com', 'sales');
    mysql> INSERT INTO test (name, email, department) VALUES ('bob', 'bob@abc.com', 'sales');
    mysql> INSERT INTO test (name, email, department) VALUES ('bob', 'bob@abc.com', 'sales');
    mysql> INSERT INTO test (name, email, department) VALUES ('bob', 'bob@abc.com', 'sales');
    mysql> INSERT INTO test (name, email, department) VALUES ('bob', 'bob@abc.com', 'sales');
    mysql> INSERT INTO test (name, email, department) VALUES ('bob', 'bob@abc.com', 'sales');
    mysql> CREATE DATABASE test;
    mysql> USE test;
    mysql> CREATE TABLE customers (id INT NOT NULL AUTO_INCREMENT, name VARCHAR(255), PRIMARY KEY (id));
    ```   
3. Create a connector by running:
    ```bash
       $ ./run_mysql_connector.sh
        ```
4. Verify that connector is running using Kafka Connect UI: http://localhost:8002/  
5. Verrify data in Kafka topic using Conduktor
